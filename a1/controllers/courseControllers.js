import Course from '../models/Course.js';


export const getCourses = async (req, res) => {
    const courses = await Course.find();
    res.status(200).json(courses);
}

export const createCourse = async (req, res) => {
    const result = await Course.findOne({courseName: req.body.courseName});
    if(result === null){
        const newCourse = new Course(req.body);
        newCourse.save((err, result) => {
            if(err) {
                return res.send(err);
            } else {
                return res.send(`new course: ${result}`);
            }
        });
    } else {
        return res.status(404).send(`Course already exist!`);
    }
}

export const getActiveCourses = async (req, res) => {
    const activeCourses = await Course.find({isActive: true});
    if(activeCourses) {
        res.status(200).send(activeCourses);
    } else {
        return false
    }
}

export const findCourse = async (req, res) => {
    const result = await Course.findOne({courseName: req.body.courseName});

    if(result){
        res.status(200).send(result);
    } else {
        res.status(404).send(`${req.body.courseName} does not exist`);
    }
}

export const getCourse = async (req, res) => {
    const result = await Course.findById(req.params.id);

    if(result){
        res.status(200).send(result);
    } else {
        res.status(404).send(`Course does not exist`);
    }
}

export const updateCourseActive = async (req, res) => {
    const result = await Course.findById(req.params.courseId);
    if(result) {
        const updateCourse = await Course.findByIdAndUpdate(req.params.courseId, { isActive: !result.isActive }, {new: true});
        res.status(202).send(`${result.courseName} isActive property to ${updateCourse.isActive} is updated!`)
    } else {
        res.status(404).send(`Course does not exist!`);
    }
}

// export const archiveCourse = async (req, res) => {
    
// }