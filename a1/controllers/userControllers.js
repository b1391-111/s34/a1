import User from '../models/User.js';
import bcrypt from 'bcrypt';
import { createAccessToken, decode } from '../auth.js';


export const getUsers = async (req, res) => {
    const users = await User.find();
    if(users) {
        res.status(200).json(users);
    } else {
        res.status(404).send("Error");
    }
}


export const registerUser = async (req, res) => {
    const result = await User.findOne({email: req.body.email});
    if(result === null){
        const salt = 12;
        const userPassword = req.body.password;
        const hashedPassword = await bcrypt.hash(userPassword, salt);
        const newUser = {
            ...req.body,
            password: hashedPassword
        }
        const registeredUser = new User(newUser);
        registeredUser.save((err, result) => {
            if(err) {
                return res.send(err);
            } else {
                return res.send(`new user: ${result}`);
            }
        });
    } else {
        return res.status(404).send(`User already exist!`);
    }
}


export const loginUser = async (req, res) => {
    const { email, password } = req.body;
    const dbUser = await User.findOne({email});
    
    if(dbUser){
        const isPassValid = await bcrypt.compare(password, dbUser.password);
        if(isPassValid) {
            const token = createAccessToken(dbUser);
            res.status(200).json({ result: dbUser, token });
        } else {
            res.status(404).send(`Invalid Password`)
        }
    } else {
        res.status(404).send(`Invalid Credentials`);
    }
}

export const getProfile = async (req, res) => {
    let userData = decode(req.headers.authorization.split(" ")[1]);
    let dbUser = await User.findById(userData._id);
    if(dbUser){
        dbUser.password = "";
        res.status(200).json(dbUser);
    } else {
        res.status(404).send("User does not exist!");
    }
}